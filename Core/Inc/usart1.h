#ifndef STM32F1XX_USART1_H
#define STM32F1XX_USART1_H


#include "stm32f1xx.h"

#define USART1_RXBUFF_SIZE 64            //max 256
#define USART1_TXBUFF_SIZE 64            //max 256

#define USART1_RXBUFF_MASK	(USART1_RXBUFF_SIZE - 1)
#define USART1_TXBUFF_MASK	(USART1_TXBUFF_SIZE - 1)


void USART1_init(void);
uint8_t USART1_getc(void);
void USART1_putc(uint8_t);
void USART1_puts(char *s);
uint8_t USART1_available(void);
void USART1_flush(void);



#endif
