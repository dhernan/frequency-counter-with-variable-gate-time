#include "main.h"

#define HZ 	0
#define KHZ	1
#define MHZ 2


void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART1_UART_Init(void);


void show_frequency(uint8_t xpos, uint8_t ypos, double actual_frequency);

const uint16_t time_table[60] =
{
		100,   200,   300,   400,   500,   600,   700,   800,   900,  1000, 		//00 - 09
		2000,  3000,  4000,  5000,  6000,  7000,  8000,  9000, 10000, 11000, 		//10 - 19
		12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000, 21000, 		//20 - 29
		22000, 23000, 24000, 25000, 26000, 27000, 28000, 29000, 30000, 31000, 		//30 - 39
		32000, 33000, 34000, 35000, 36000, 37000, 38000, 39000, 40000, 41000, 		//40 - 49
		42000, 43000, 44000, 45000, 46000, 47000, 48000, 49000, 50000, 60000 		//50 - 59
};


extern volatile uint32_t reference;
extern volatile uint32_t raw_freq;
extern volatile uint8_t measurement_ready;
extern volatile uint16_t timeout;
extern volatile uint8_t gate_time;
extern volatile uint32_t idle_time;
extern volatile uint8_t clear_data;
extern volatile uint16_t wait_time;
extern volatile uint8_t show_wait_time;


int main(void)
{
	char buffer[40];
	double ratio = 0.0;
	double cpu_clock = 70000000.0;							//70 MHz, we cannot go higher because
	double actual_frequency = 0.0;							//the maximum gate-time is 60s, that
	double average_frequency = 0.0;							//makes the TIM1,TIM2 count almost 2^32

	double samples[20];										//store the most recent 20 measurements
	uint16_t tmp = 0;
	uint8_t head_sampl;
	uint8_t nr_sampl;
	uint8_t show_gate_time;

	//starting values
	gate_time = 9;											//lookup in table
	timeout = time_table[gate_time];						//it is 1 second
	idle_time = timeout * 2;								//2 seconds
	show_gate_time = 1;										//show gate time without turning the knob
	head_sampl = 0;
	nr_sampl = 0;

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	LL_GPIO_AF_Remap_SWJ_NOJTAG();

	SystemClock_Config();
	MX_GPIO_Init();
	MX_TIM4_Init();
	MX_TIM1_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_USART1_UART_Init();

	//USART1_init();										//not needed, no incoming serial data
	us_DELAY_init();
	HD44780_init(HD44780_DISPLAY_ON_CURSOR_OFF_BLINK_OFF);
	HD44780_clrscr();

	LL_SYSTICK_EnableIT();
	LL_TIM_EnableIT_CC1(TIM1);

	LL_TIM_CC_EnableChannel(TIM1, LL_TIM_CHANNEL_CH1);
	LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH1);
	LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH1);
	LL_TIM_CC_EnableChannel(TIM4, LL_TIM_CHANNEL_CH1);

	LL_TIM_EnableCounter(TIM1);
	LL_TIM_EnableCounter(TIM2);
	LL_TIM_EnableCounter(TIM3);
	LL_TIM_EnableCounter(TIM4);

	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_11);			//prepare for measurement by
	//														//setting the D-flipflop LOW

	while (1)
	{
		if (measurement_ready)								//start calculation of real frequencies
		{
			measurement_ready = 0;

			ratio = cpu_clock / (double) reference;			//current frequency
			actual_frequency = (double) raw_freq * ratio;

			show_frequency(0, 0, actual_frequency);			//send to 1st line of display

			samples[head_sampl] = actual_frequency;			//store samples in array (20)
			if (head_sampl < 19) head_sampl++;
			else head_sampl = 0;
			if (nr_sampl < 20) nr_sampl++;

			average_frequency = 0.0;						//calculate the average frequency
			for (tmp = 0; tmp < nr_sampl; tmp++) average_frequency += samples[tmp];
			average_frequency/= nr_sampl;

			show_frequency(0, 3, average_frequency);		//send to 4th line of display

			show_gate_time = 1;

			timeout = time_table[gate_time];				//start next measurement
			idle_time = timeout * 2;						//do not wait too long for input signal
			LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_11);	//prepare for next measurement by
			//												//setting the D-flipflop LOW again
		}

		if (clear_data == 1)								//rotary button was pressed, so reset all
		{
			clear_data = 0;

			head_sampl = 0;									//clear array
			nr_sampl = 0;
			for (tmp = 0; tmp < 20; tmp++)
			{
				samples[tmp] = 0.0;
			}

			actual_frequency = 0.0;
			average_frequency = 0.0;

			show_gate_time = 1;								//show cleared data
			show_frequency(0, 0, actual_frequency);
			show_frequency(0, 3, average_frequency);
		}


		if (show_gate_time == 1)							//show gate-time when new measurement
			//												//and when new gate-time selected
		{
			show_gate_time = 0;

			HD44780_gotoxy(0, 1);
			HD44780_puts("gatetime        ");

			if (time_table[gate_time] < 1000)
			{
				HD44780_gotoxy(9, 1);
				sprintf(buffer, "%u ms", time_table[gate_time]);
			}
			else
			{
				if (time_table[gate_time] < 10000) HD44780_gotoxy(11, 1);
				else HD44780_gotoxy(10, 1);
				sprintf(buffer, "%u s", time_table[gate_time] / 1000);
			}
			HD44780_puts(buffer);
		}

		if (show_wait_time == 1)							//for the longer gate-times it shows
			//												//the remaining wait time
		{
			show_wait_time = 0;
			HD44780_gotoxy(0, 2);
			HD44780_puts("sampling        ");

			if (time_table[gate_time] < 1000)				//gate-times lower than 1s are shown
				//											//as the gate-time itself
			{
				HD44780_gotoxy(9, 2);
				sprintf(buffer, "%u ms", time_table[gate_time]);
			}
			else
			{
				if (wait_time < 10) HD44780_gotoxy(11, 2);	//for gate-times of 1s and higher the
				else HD44780_gotoxy(10, 2);					//remaining seconds are counted down
				sprintf(buffer, "%u s", wait_time);
			}
			HD44780_puts(buffer);
		}


		if (idle_time == 0)									//probably no input signal
		{
			HD44780_gotoxy(0, 0);
			HD44780_puts("wait for signal ");

			LL_mDelay(99);

			timeout = time_table[gate_time];				//start next measurement
			idle_time = timeout * 2;						//do not wait longer for input signal
			LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_11);	//prepare for next measurement
		}
	}
}



void show_frequency(uint8_t xpos, uint8_t ypos, double frequency)
{
	char buffer[40];
	uint8_t range;
	uint8_t symbol = 0;
	uint8_t position_digpoint = 0;
	double multiplier= 0.0;
	uint32_t display_frequency = 0;
	uint8_t digits[9];										//alle losse cijfers
	uint8_t digit_nr = 0;
	char serial_buffer[13];									//9 digits + decimalpoint + \r + \n + \0
	uint8_t serial_digit;

	if      (frequency < 1.0) range = 0;
	else if (frequency < 10.0) range = 1;
	else if (frequency < 100.0) range = 2;
	else if (frequency < 1000.0) range = 3;
	else if (frequency < 10000.0) range = 4;
	else if (frequency < 100000.0) range = 5;
	else if (frequency < 1000000.0) range = 6;
	else if (frequency < 10000000.0) range = 7;
	else range = 8;

	switch (range)
	{
	case 0:											// < 1 Hz
		multiplier = 100000000.0;					//for the less than 1 Hz range the multiplier
		symbol = HZ;								//is equal to the < 10 Hz range, that way the
		position_digpoint = 0;						//most significant digit becomes a zero.
		break;
	case 1:											// < 10 Hz
		multiplier = 100000000.0;
		symbol = HZ;
		position_digpoint = 0;
		break;
	case 2:											// < 100 Hz
		multiplier = 10000000.0;
		symbol = HZ;
		position_digpoint = 1;
		break;
	case 3:											// < 1000 Hz
		multiplier = 1000000.0;
		symbol = HZ;
		position_digpoint = 2;
		break;
	case 4:											// < 10 kHz
		multiplier = 100000.0;
		symbol = KHZ;
		position_digpoint = 0;
		break;
	case 5:											// < 100 kHz
		multiplier = 10000.0;
		symbol = KHZ;
		position_digpoint = 1;
		break;
	case 6:											// < 1000 kHz
		multiplier = 1000.0;
		symbol = KHZ;
		position_digpoint = 2;
		break;
	case 7:											// < 10 MHz
		multiplier = 100.0;
		symbol = MHZ;
		position_digpoint = 0;
		break;
	case 8:											// < 100 MHz
		multiplier = 10.0;
		symbol = MHZ;
		position_digpoint = 1;
		break;
	default:
		//											// error
		break;
	}

	HD44780_gotoxy(xpos, ypos);

	display_frequency = (uint32_t) (round (frequency * multiplier));
	for (digit_nr = 0; digit_nr < 9; digit_nr++)	//store separated digits in array (digits[])
	{
		digits[8 - digit_nr] = display_frequency % 10;
		display_frequency /= 10;
	}

	for (digit_nr = 0; digit_nr < 9; digit_nr++)	//send separated digits to display
	{												//and place the digital-point and
		sprintf(buffer, "%d", digits[digit_nr]);	//commas in the right places
		HD44780_puts(buffer);
		if (digit_nr == position_digpoint) HD44780_puts(".");
		if (digit_nr == (position_digpoint + 3)) HD44780_puts(",");
		if (digit_nr == (position_digpoint + 6))
		{
			if (digit_nr < 8) HD44780_puts(",");
		}
	}

	if (ypos == 0)									//ypos is 0 for the actual-frequency
	{												//send this frequency to the serial port
		digit_nr = 0;
		serial_digit = 0;

		if (range == 0) range = 1;					//place digital-point for freq < 1 Hz in
		//											//the same place as for freq < 10 Hz

		while (digit_nr < 9)						//store digits in buffer and add the
		{											//CR LF and the \0 char to end the string
			serial_buffer[serial_digit++] = digits[digit_nr++] + 0x30;
			if (serial_digit == range)
			{
				serial_buffer[serial_digit++] = '.';
			}
		}
		serial_buffer[serial_digit++] = '\r';
		serial_buffer[serial_digit++] = '\n';
		serial_buffer[serial_digit] = '\0';
		USART1_puts(serial_buffer);
	}

	if (symbol == HZ)  HD44780_puts(" Hz ");
	if (symbol == KHZ) HD44780_puts(" kHz");
	if (symbol == MHZ) HD44780_puts(" MHz");
}


void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
	while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_2);
	LL_RCC_HSE_EnableBypass();
	LL_RCC_HSE_Enable();
	while(LL_RCC_HSE_IsReady() != 1);
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_7);
	LL_RCC_PLL_Enable();
	while(LL_RCC_PLL_IsReady() != 1);
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);
	LL_Init1msTick(70000000);
	LL_SetSystemCoreClock(70000000);
}


static void MX_TIM1_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM1);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_12;						//ETR2
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_8;						//CH1
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	NVIC_SetPriority(TIM1_CC_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM1_CC_IRQn);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 65535;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	TIM_InitStruct.RepetitionCounter = 0;
	LL_TIM_Init(TIM1, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM1);
	LL_TIM_ConfigETR(TIM1, LL_TIM_ETR_POLARITY_NONINVERTED, LL_TIM_ETR_PRESCALER_DIV1, LL_TIM_ETR_FILTER_FDIV1);
	LL_TIM_SetClockSource(TIM1, LL_TIM_CLOCKSOURCE_EXT_MODE2);

	LL_TIM_SetTriggerOutput(TIM1, LL_TIM_TRGO_UPDATE);
	LL_TIM_DisableMasterSlaveMode(TIM1);

	LL_TIM_IC_SetActiveInput(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
	LL_TIM_IC_SetPolarity(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
}


static void MX_TIM2_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;					//CH1
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM2_IRQn);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 65535;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM2, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM2);
	LL_TIM_SetTriggerInput(TIM2, LL_TIM_TS_ITR0);
	LL_TIM_SetSlaveMode(TIM2, LL_TIM_CLOCKSOURCE_EXT_MODE1);
	LL_TIM_DisableIT_TRIG(TIM2);
	LL_TIM_DisableDMAReq_TRIG(TIM2);
	LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM2);

	LL_TIM_IC_SetActiveInput(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
	LL_TIM_IC_SetPolarity(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
}


static void MX_TIM3_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_6;					//CH1
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	NVIC_SetPriority(TIM3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM3_IRQn);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 65535;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM3, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM3);
	LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_UPDATE);
	LL_TIM_DisableMasterSlaveMode(TIM3);

	LL_TIM_IC_SetActiveInput(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
	LL_TIM_IC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
}


static void MX_TIM4_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_6;					//CH1
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	NVIC_SetPriority(TIM4_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM4_IRQn);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 65535;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM4, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM4);
	LL_TIM_SetTriggerInput(TIM4, LL_TIM_TS_ITR2);
	LL_TIM_SetSlaveMode(TIM4, LL_TIM_CLOCKSOURCE_EXT_MODE1);
	LL_TIM_DisableIT_TRIG(TIM4);
	LL_TIM_DisableDMAReq_TRIG(TIM4);
	LL_TIM_SetTriggerOutput(TIM4, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM4);

	LL_TIM_IC_SetActiveInput(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
	LL_TIM_IC_SetPolarity(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
}


static void MX_USART1_UART_Init(void)
{
	LL_USART_InitTypeDef USART_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_9;					//TXD
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_10;					//RXD
	GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;					//to prevent noise when not connected to anything
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(USART1_IRQn);

	USART_InitStruct.BaudRate = 38400;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART1, &USART_InitStruct);

	LL_USART_ConfigAsyncMode(USART1);
	LL_USART_Enable(USART1);
}


static void MX_GPIO_Init(void)
{
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_1);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_2);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_3);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_11);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_15);
	LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_7);
	LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_8);
	LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_9);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1 | LL_GPIO_PIN_2 | LL_GPIO_PIN_3 | LL_GPIO_PIN_4;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;				//LCD DATA
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_11;					//D flipflop
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	GPIO_InitStruct.Pin = LL_GPIO_PIN_15;					//100 Hz output
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	GPIO_InitStruct.Pin = LL_GPIO_PIN_7 | LL_GPIO_PIN_8 | LL_GPIO_PIN_9;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;				//LCD CONTROL
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	GPIO_InitStruct.Pin = LL_GPIO_PIN_11;					//rotary encoder
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_12;					//rotary encoder
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_13;					//rotary encoder
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}


void Error_Handler(void)
{
	__disable_irq();
	while (1)
	{
	}
}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif
