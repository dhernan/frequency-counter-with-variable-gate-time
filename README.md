# Frequency counter with variable gate time

Reciprocal frequency counter with variable gate time
Gate time selectable from 0.1 second up to 60 seconds
Displays current measurement and average of last 20 measurements
3.3V input (5V safe)